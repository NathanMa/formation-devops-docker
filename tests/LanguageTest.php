<?php

require_once __DIR__ . '/../src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../src/Language.php");

class LanguageTest extends \PHPUnit\Framework\TestCase
{
   public function testgetLanguage()
   {
       $city = new Language();
       $result = $city->GetLanguage('FR');
       $expected = 'Français';
       $this->assertTrue($result == $expected);
   }
}
